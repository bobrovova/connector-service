const addresses = require('./collections/addresses');
const tokens = require('./collections/tokens');
const watchTransactions = require('./controllers/transaction/transactions').watchTransactions;


(async () => {
    await addresses.fetchAddresses();
    await tokens.fetchTokens();

    addresses.updateBalances();
    watchTransactions();
})();