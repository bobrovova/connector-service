const _ = require('underscore');
const web3 = require('./../config/web3');

class Address {
    constructor(options){
        this._address = options.address;
        this._eth = options.eth;
        this._tokens = options.tokens;
    }

    getAddress(){
        return this._address;
    }

    getTokens(){
        return this._tokens;
    }

    getTokenByIndex(index){
        return this._tokens[index];
    }

    setTokenValueByIndex(index, value){
        this._tokens[index].value = value;
    }

    setBalanceToken(addressToken, balance){
        let indexToken = _.findIndex(this._tokens, (token) => {
            if(token.tokenAddress == addressToken){
                return true;
            }
            return false;
        }, addressToken);

        this._tokens[indexToken].value = balance;
    }

    setEth(value) {
        this._eth = value;
    }

    getEth(){
        return this._eth;
    }
}

module.exports = Address;