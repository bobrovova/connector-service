class Token {
    constructor(address){
        this._address = address;
    }

    getAddress(){
        return this._address;
    }
}

module.exports = Token;