const Transaction = require('./../transactions/transaction');

class BaseParseStrategy {
    constructor(){
        this._typeTransaction = Transaction;
    }

    getTypeTransaction(){
        return this._typeTransaction;
    }
}

module.exports = BaseParseStrategy;