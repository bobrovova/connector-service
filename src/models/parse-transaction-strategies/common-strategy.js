const web3 = require('./../../config/web3');
const BaseParseStrategy = require('./base-parse-strategy');

class CommonStrategy extends BaseParseStrategy {
    constructor(){
        super();
    }

    parse(transaction){
        return {
            to: transaction.to,
            value: transaction.value,
            from: transaction.from
        }
    }
}

module.exports = CommonStrategy;