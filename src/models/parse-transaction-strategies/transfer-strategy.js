const web3 = require('./../../config/web3');
const TokenTransferTransaction = require('./../transactions/token-transfer-transaction');
const BaseParseStrategy = require('./base-parse-strategy');

class TransferStrategy extends BaseParseStrategy {
    constructor(){
        super();
        this._typeTransaction = TokenTransferTransaction;
    }

    parse(transaction){
        let to = '0x' + transaction.input.substr(34, 40);
        let value = web3.toBigNumber('0x' + transaction.input.substr(74, 64)).toString();
        return {
            to: to,
            value: value,
            from: transaction.from,
            tokenAddress: transaction.to,
            command: 'transfer'
        }
    }
}

module.exports = TransferStrategy;