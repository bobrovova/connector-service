const TransferStrategy = require('./transfer-strategy');
const CommonStrategy = require('./common-strategy');

const strategies = {
    transfer: TransferStrategy,
    common: CommonStrategy
}

function createParseStrategy(command){
    return new strategies[command]();
}

module.exports = { createParseStrategy };