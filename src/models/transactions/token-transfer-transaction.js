const _ = require('underscore');
const Transaction = require('./transaction');
const addresses = require('./../../collections/addresses');
const tokens = require('./../../collections/tokens');
const web3 = require('./../../config/web3');

class TokenTransferTransaction extends Transaction {
    constructor(options){
        super(options);
        this._tokenAddress = options.tokenAddress;
    }

    findConnectedAddresses(){
        return _.filter(addresses.getAddresses(), (address) => {
            if(this._to == address.getAddress() || this._from == address.getAddress()){
                return _.some(tokens.getTokens(), (token) => {
                    if(this._tokenAddress == token.getAddress()) return true;
                    return false;
                }, this);
            }
            return false;
        }, this);
    }

    setTokenAddress(tokenAddress){
        this._tokenAddress = tokenAddress;
    };

    getTokenAddress(){
        return this._tokenAddress;
    }

    execute(){
        let connectedAddresses = this.findConnectedAddresses();
        let signals = [];
        connectedAddresses.forEach((address, i, arr) => {
            let diff = web3.toBigNumber(this._value);
            if(this._from == address.getAddress()){
                diff.multiple(-1);
            }

            let tokenIndex = _.findIndex(address.getTokens(), (token) => {
                if(token.tokenAddress == this._tokenAddress) return true;
                return false;
            }, this);

            address.setTokenValueByIndex(
                tokenIndex,
                web3.toBigNumber(address.getTokenByIndex(tokenIndex).value).plus(diff).toString()
            );

            signals.push(this.createSignal());
        }, this);

        return {
            signals: signals
        }
    }
}

module.exports = TokenTransferTransaction;