const _ = require('underscore');
const tokens = require('./../../collections/tokens');
const addresses = require('./../../collections/addresses');
const web3 = require('./../../config/web3');
const BaseSignal = require('./../signals/base-signal');

class Transaction {
    constructor(options){
        this._from = options.from;
        this._to = options.to;
        this._value = options.value;
        this._typeSignal = BaseSignal;
    }

    findConnectedAddresses(){
        return _.filter(addresses.getAddresses(), (address) => {
            if(this._to == address.getAddress() || this._from == address.getAddress()){
                return true;
            }
            return false;
        }, this);
    }

    getFrom(){
        return this._from;
    }

    setFrom(from){
        this._from = from;
    }

    getTo(){
        return this._to;
    }

    setTo(to){
        this._to = to;
    }

    getValue(){
        return this._value;
    }

    setValue(){
        this._value = value;
    }

    createSignal(){
        return new (this._typeSignal)();
    }

    execute(){
        let connectedAddresses = this.findConnectedAddresses();
        let signals = [];
        connectedAddresses.forEach((address, i, arr) => {
            let diff = web3.toBigNumber(this._value);
            if(this._from == address.getAddress()){
                diff.multiple(-1);
            }

            address.setEth(web3.toBigNumber(address.getEth()).plus(diff).toString());
            signals.push(this.createSignal());
        }, this);

        return {
            signals: signals
        }
    }
}

module.exports = Transaction;