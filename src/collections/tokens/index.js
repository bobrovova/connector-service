const request = require('request');
const log = require('winston');
const Token = require('./../../models/token');

class Tokens {
    constructor(){
        this._tokens = [];
    }

    async fetchTokens(){
        //this._tokens = await request('http://localhost:7777/tokens/');
        this._tokens = [
            new Token('0xF433089366899D83a9f26A773D59ec7eCF30355e')
        ];

        log.info('Tokens has been fetched');
    }

    getTokens(){
        return this._tokens;
    }
}

const tokens = new Tokens();

module.exports = tokens;