const request = require('request');
const log = require('winston');
const Address = require('./../../models/address');
const web3 = require('./../../config/web3');
const createCallRequestBalanceOf = require('./../../utils/create-call-request-balance');

class Addresses {
    constructor(){
        this._addresses = [];
    }

    async fetchAddresses(){
        //let addresses = await request('http://localhost:7777/addresses/');
        let addresses = [
            {
                address: '0xfbb1b73c4f0bda4f67dca266ce6ef42f520fbb98',
                eth: '0',
                tokens: [
                    {
                        tokenAddress: '0xF433089366899D83a9f26A773D59ec7eCF30355e',
                        value: '3'
                    }
                ]
            }
        ];

        log.info('Addresses has been fetched');

        this._addresses = addresses.map((item, i, arr) => {
            return new Address(item);
        });
    }

    updateBalances(){
        this._addresses.forEach((address, i, arr) => {
            address.setEth(web3.eth.getBalance(address.getAddress()).toString());
            address.getTokens().forEach((token, i, arr) => {
                let balance = web3.toBigNumber(
                    web3.eth.call(createCallRequestBalanceOf(address, token.tokenAddress))
                ).toString();
                address.setBalanceToken(token.tokenAddress, balance);
            }, address);
        });
    }

    getAddresses(){
        return this._addresses;
    }
}

const addresses = new Addresses();

module.exports = addresses;