module.exports = function createCallRequestBalanceOf(address, tokenAddress){
    let data = '0x70a08231' + '000000000000000000000000' + address.getAddress().substr(2, 42);

    return {
        to: tokenAddress,
        data: data
    }
}