const Transaction = require('./../../models/transactions/transaction');
const web3 = require('./../../config/web3');
const TokenTransferTransaction = require('./../../models/transactions/token-transfer-transaction');
const createParseStrategy = require('./../../models/parse-transaction-strategies/parse-transaction').createParseStrategy;

const filter = web3.eth.filter('latest');

function pushSignals(signals) {
    signals.forEach((signal, i, arr) => {
        signal.push();
    });
}

function processTransactions(rawTransactions){
    rawTransactions.forEach((rawTransaction, i, arr) => {
        if(rawTransaction.input != undefined) {
            let command = rawTransaction.input.substr(0, 10);

            if (command == '0xa9059cbb') {
                let strategy = createParseStrategy('transfer');
                let transaction = new (strategy.getTypeTransaction())(strategy.parse(rawTransaction));
                let result = transaction.execute();
                pushSignals(result.signals);
            }
        }

        if(rawTransaction.value != '0'){
            let strategy = createParseStrategy('common');
            let transaction = new (strategy.getTypeTransaction())(strategy.parse(rawTransaction));
            let result = transaction.execute();
            pushSignals(result.signals);
        }
    });
}

function watchTransactions(){
    filter.watch(function onReceiveHashBlock(error, hashBlock){
        web3.eth.getBlock(hashBlock, true, function onReceiveBlock(error, block){
            let testTxs = [
                {
                    to: '0xfbb1b73c4f0bda4f67dca266ce6ef42f520fbb90',
                    from: '0x8d12A197cB00D4747a1fe03395095ce2A5CC6812',
                    value: '56354651342345655582'
                },
                {
                    to: '0xF433089366899D83a9f26A773D59ec7eCF30355e',
                    from: '0x8d12A197cB00D4747a1fe03395095ce2A5CC6812',
                    value: '0',
                    input: '0xa9059cbb000000000000000000000000fbb1b73c4f0bda4f67dca266ce6ef42f520fbb980000000000000000000000000000000000000000000000000000000035772240'
                }
            ];

            processTransactions(testTxs);
        });
    });
}

module.exports = { processTransactions, watchTransactions };